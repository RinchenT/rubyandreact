Rails.application.routes.draw do

  root 'albums#index'
  get 'albums', to: 'albums#index'
  namespace :api do
    namespace :v1 do
      resources :albums, only: [:index, :show]
      resources :photos, only: [:index, :show]
      resources :users, only: [:index, :show]
    end
  end

  match '*path', to: 'albums#index', via: :all
end
