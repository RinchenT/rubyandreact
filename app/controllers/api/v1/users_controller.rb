class Api::V1::UsersController < Api::V1::BaseController
    include HTTParty
    base_uri 'jsonplaceholder.typicode.com/'
    respond_to :json
    
    def index
      user_id = params[:id]

      if user_id
        @users = self.class.get('/users?id=' + user_id)
      else
        @users = self.class.get('/users')
      end
      
      render json: @users.body  
    end

    def show
    end
  end
