class Api::V1::PhotosController < Api::V1::BaseController
    include HTTParty
    base_uri 'jsonplaceholder.typicode.com/'
    respond_to :json
    
    def index
      albumId = params[:albumId]

      if albumId
        @photos = self.class.get('/photos?albumId=' + albumId)
      else
        @photos = self.class.get('/photos')
      end
      render json: @photos.body  
    end

    def show
    end
end