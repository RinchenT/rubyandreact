 class Api::V1::AlbumsController < Api::V1::BaseController
    include HTTParty
    base_uri 'jsonplaceholder.typicode.com/'
    respond_to :json
    
    def index
      album_id = params[:id]
      if album_id
        @albums = self.class.get('/albums?id='+album_id)
      else
        @albums = self.class.get('/albums')
      end
      render json: @albums.body  
    end

    def show
    end
  end
