import React from "react";
import { Route, Switch } from "react-router-dom";
import Albums from "./Albums";
import User from "./User";
import Album from "./Album";

class App extends React.Component {
  render() {
    return (
      <div>
        <Switch>
          <Route exact path="/" component={Albums} />
          <Route exact path="/albums" component={Albums} />
          <Route exact path="/albums/:id" component={Album} />
          <Route path="/users/:id" component={User} />
        </Switch>
      </div>
    );
  }
}

export default App;
