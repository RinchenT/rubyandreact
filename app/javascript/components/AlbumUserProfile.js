import React from "react";
import { Link } from "react-router-dom";

export default (props) => {
  const { user } = props;
  return (
    <div key={user.id} className="jumbotron">
      <div className="container">
        <Link to="/albums" className="btn btn-primary">
          Back
        </Link>
        <div className="row">
          <div className="col-md-6">
            <h2>User Details</h2>
            <p>
              <Link to={`/users/${user.id}`}>{user.name}</Link>
            </p>
            <p>{user.email}</p>
            <p>{user.phone}</p>
          </div>
          <div className="col-md-6">
            <h2>Address</h2>
            {user.address && (
              <div>
                <p>{user.address.street}</p>
                <p>{user.address.suite}</p>
                <p>{user.address.city}</p>
                <p>{user.address.zipcode}</p>
              </div>
            )}
          </div>
        </div>
      </div>
    </div>
  );
};
