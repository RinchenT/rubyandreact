import React from "react";

class User extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      id: props.match.params.id,
      user: {},
    };
  }

  componentDidMount() {
    fetch(`/api/v1/users.json?id=${this.state.id}`)
      .then((response) => {
        return response.json();
      })
      .then((data) => {
        this.setState({ user: data[0] });
      });
  }

  render() {
    let user = this.state.user;
    return (
      <div className="container">
        <div className="jumbotron">
          <button
            className="btn btn-primary"
            onClick={this.props.history.goBack}
          >
            Back
          </button>

          {user && (
            <div>
              <h1>{user.name}</h1>
              <p>Username: {user.username}</p>
              <p>Email: {user.email}</p>
              <p>Phone: {user.phone}</p>
              <p>Website: {user.website}</p>

              {user.address && (
                <div className="address">
                  <h2>Address</h2>
                  <p>{user.address.street}</p>
                  <p>{user.address.suite}</p>
                  <p>{user.address.city}</p>
                  <p>{user.address.zipcode}</p>
                  <p>{user.address.geo.lat}</p>
                  <p>{user.address.geo.lng}</p>
                  <iframe
                    width="100%"
                    height="450"
                    src={
                      "https://www.google.com/maps/embed/v1/place?q=" +
                      user.address.geo.lat +
                      "," +
                      user.address.geo.lng +
                      " &key=AIzaSyDBZWpsCpSGIIEuO5UE48WL4P-Cu6H6MHc"
                    }
                  ></iframe>
                </div>
              )}

              {user.company && (
                <div className="company">
                  <h2>Company</h2>
                  <p>{user.company.name}</p>
                  <p>{user.company.catchPhrase}</p>
                  <p>{user.company.bs}</p>
                </div>
              )}
            </div>
          )}
        </div>
      </div>
    );
  }
}

export default User;
