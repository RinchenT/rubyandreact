import React from "react";
import AlbumUserProfile from "./AlbumUserProfile";
import AlbumPhoto from "./AlbumPhoto";
import Pagination from "react-js-pagination";

class Album extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      id: props.match.params.id,
      photos: [],
      album: [],
      user: [],
      activePage: 1,
    };
  }

  componentDidMount() {
    fetch(`/api/v1/albums.json?id=${this.state.id}`)
      .then((response) => {
        return response.json();
      })
      .then((data) => {
        fetch(`/api/v1/users.json?id=${data[0].userId}`)
          .then((response) => {
            return response.json();
          })
          .then((data) => {
            this.setState({ user: data[0] });
          });
      });
    fetch(`/api/v1/photos.json?albumId=${this.state.id}`)
      .then((response) => {
        return response.json();
      })
      .then((data) => {
        this.setState({ photos: data });
      });
  }

  handlePageChange(pageNumber) {
    this.setState({ activePage: pageNumber });
  }

  render() {
    let first_set =
      (this.state.activePage - 1) * 10 + (this.state.activePage - 1);

    return (
      <div>
        <AlbumUserProfile user={this.state.user} />
        <div className="container">
          <div className="album-list">
            {this.state.photos
              .slice(
                this.state.activePage === 1 ? 0 : first_set,
                first_set + 10
              )
              .map((photo) => {
                return (
                  <div className="album" key={photo.id}>
                    <AlbumPhoto photo={photo} />
                  </div>
                );
              })}
          </div>

          <div className="pagination">
            <Pagination
              activePage={this.state.activePage}
              itemsCountPerPage={10}
              totalItemsCount={this.state.photos.length}
              pageRangeDisplayed={5}
              onChange={this.handlePageChange.bind(this)}
            />
          </div>
        </div>
      </div>
    );
  }
}

export default Album;
