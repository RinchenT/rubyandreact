import React from "react";
import { Link } from "react-router-dom";
import Pagination from "react-js-pagination";
import "../../assets/stylesheets/album.scss";

class Albums extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      albums: [],
      users: [],
      activePage: 1,
    };
  }

  componentDidMount() {
    fetch("/api/v1/albums.json")
      .then((response) => {
        return response.json();
      })
      .then((data) => {
        this.setState({ albums: data });
      });

    fetch("/api/v1/users.json")
      .then((response) => {
        return response.json();
      })
      .then((data) => {
        this.setState({ users: data });
      });
  }

  handlePageChange(pageNumber) {
    this.setState({ activePage: pageNumber });
  }

  render() {
    let userName = "";

    let first_set =
      (this.state.activePage - 1) * 10 + (this.state.activePage - 1);
    var albums = this.state.albums
      .slice(this.state.activePage === 1 ? 0 : first_set, first_set + 10)
      .map((album) => {
        userName = this.state.users.find((user) => user.id === album.userId);
        return (
          <div className="card album" key={album.id}>
            <div className="card-body">
              <h5 className="card-title">
                <Link to={`/albums/${album.id}`} className="album__title">
                  {album.title}
                </Link>
              </h5>
              <p className="card-text">{userName && userName.name}.</p>
            </div>
          </div>
        );
      });

    return (
      <div className="album-list">
        <div className="container">
          <h1 className="text-center">List of Albums</h1>
          <div className="row">
            {albums}
            <div className="pagination">
              <Pagination
                activePage={this.state.activePage}
                itemsCountPerPage={10}
                totalItemsCount={this.state.albums.length}
                pageRangeDisplayed={5}
                onChange={this.handlePageChange.bind(this)}
              />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Albums;
