import React from "react";

export default (props) => {
  const { photo } = props;
  return (
    <div className="card album">
      <div className="card-body">
        <h5 className="card-title">{photo.title}</h5>
        <p className="card-text">
          <img src={photo.thumbnailUrl} />
        </p>
      </div>
    </div>
  );
};
